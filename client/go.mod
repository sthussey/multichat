module gitlab.com/sthussey/multichat/client

go 1.13

require (
	github.com/golang/protobuf v1.3.2
	google.golang.org/grpc v1.25.1
  github.com/oklog/ulid v2
)
