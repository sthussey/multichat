package main

import (
	"fmt"
	"github.com/oklog/ulid/v2"
	client "gitlab.com/sthussey/multichat/client/internal/client"
	"gitlab.com/sthussey/multichat/server/pkg/model"
	"google.golang.org/grpc"
	"os"
)

func main() {
	// A fake ID
	my_id, _ := ulid.New(ulid.Now(), nil)

	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())
	opts = append(opts, grpc.WithBlock())

	conn, err := grpc.Dial("localhost:8088", opts...)
	if err != nil {
		fmt.Printf("Error: unable to connect to server: %v\n", err)
		return
	}

	defer conn.Close()
	chat_client := client.CreateClient(conn, my_id)

	msg_ch := make(chan *model.ChatMessage, 5)
	input_ch := make(chan string, 5)

	go chat_client.ReceiveMessages(msg_ch)
	go client.ChatConsole(input_ch, os.Stdin)

	for {
    var ok bool
    var msg *model.ChatMessage
    var input string
		select {
		case msg, ok = <-msg_ch:
			client.DisplayChatMessage(*msg, os.Stdout)
		case input, ok = <-input_ch:
			chat_client.SendStringMessage(input, my_id)
		}
		if !ok {
			os.Exit(0)
		}
	}
}


