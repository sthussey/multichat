package client

import (
	"bufio"
	"fmt"
	"gitlab.com/sthussey/multichat/server/pkg/model"
	"io"
)

func DisplayChatMessage(msg model.ChatMessage, console io.Writer) {
	fmt.Fprintf(console, "%s: ", msg.Source())
	for _, p := range msg.Parts() {
		fmt.Fprintf(console, "%s\n", p.String())
	}
}

func ChatConsole(input_ch chan string, console io.Reader) {
	scanner := bufio.NewScanner(console)
	scanner.Split(bufio.ScanLines) // It is the default, but seems cleaner to be explicit
	for scanner.Scan() {
		input_ch <- scanner.Text()
	}
	close(input_ch)
}
