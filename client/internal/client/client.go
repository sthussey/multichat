package client

import (
	"context"
	"fmt"
	pb "gitlab.com/sthussey/multichat/server/interface"
	"gitlab.com/sthussey/multichat/server/pkg/model"
  "github.com/oklog/ulid/v2"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
	"io"
)

type MultiChatClient struct {
	pb_client pb.MultiChatClient
  multichat_id ulid.ULID
	MessageStream   pb.MultiChat_StartStreamClient
}

func CreateClient(cc *grpc.ClientConn, client_id ulid.ULID) *MultiChatClient {
	pb_client := pb.NewMultiChatClient(cc)
	client := MultiChatClient{pb_client: pb_client, multichat_id: client_id}
	return &client
}

func (c *MultiChatClient) ReceiveMessages(msg_ch chan *model.ChatMessage) {
  err := c.InitializeStream()
  if err != nil {
    close(msg_ch)
    return
  }

  for {
    resp, err := c.MessageStream.Recv()
    if err == io.EOF {
      fmt.Printf("Server closed connection without a response.\n")
    }
    if err != nil {
      fmt.Printf("Error: could not receive server response: %v\n", err)
    }

	  switch m := resp.Message.(type) {
	  case *pb.ChatFrame_ChatMsg:
      msg, err := model.ChatMessageFromGrpc(*m.ChatMsg)
		  if err != nil {
			  fmt.Printf("Error processing chat message: %s", err)
        continue
		  }
      msg_ch <- msg
	  case *pb.ChatFrame_ControlMsg:
		  continue
	  }
  }
}

func (c *MultiChatClient) InitializeStream() error {
	if c.pb_client == nil {
		return fmt.Errorf("Chat client not initialized.\n")
	}

	if c.MessageStream == nil {
    ctx := context.Background()
    ctx = metadata.AppendToOutgoingContext(ctx, "client_id", c.multichat_id.String())
    var err error
		c.MessageStream, err = c.pb_client.StartStream(ctx)
		if err != nil {
			return fmt.Errorf("Error initiating StartStream stream: %s\n", err)
		}
	}
  return nil
}

func (c *MultiChatClient) SendStringMessage(m string, dest ulid.ULID) error {
  msg, err := model.NewChatMessage(c.multichat_id, dest)
  if err != nil {
    return fmt.Errorf("Error creating message model: %v", err)
  }
  mp, err := model.NewChatMessagePart("text/plain", []byte(m))
  msg.AddPart(mp)
  err = c.SendMessage(*msg)
  if err != nil {
    return fmt.Errorf("Error sending message: %v", err)
  }
  return nil
}

func (c *MultiChatClient) SendMessage(msg model.ChatMessage) error {
  err := c.InitializeStream()
  if err != nil {
    return fmt.Errorf("Error initializing message stream: %v", err)
  }

	gmsg, err := msg.ToGrpc()
  if err != nil {
    return fmt.Errorf("Error converting message to grpc: %v", err)
  }
  payload := pb.ChatFrame_ChatMsg{ChatMsg: gmsg}
  frame := pb.ChatFrame{Message: &payload}

	err = c.MessageStream.Send(&frame)
	if err != nil {
		return fmt.Errorf("Error: could not send chat msg: %v\n", err)
	}

	return nil
}
