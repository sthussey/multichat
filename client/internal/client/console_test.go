package client

import (
	"bytes"
	"fmt"
	"github.com/oklog/ulid/v2"
	"gitlab.com/sthussey/multichat/server/pkg/model"
	"testing"
)

func TestDisplayMessage(t *testing.T) {
	c := new(bytes.Buffer)

	s, _ := ulid.New(ulid.Now(), nil)
	d, _ := ulid.New(ulid.Now(), nil)
	m, err := model.NewChatMessage(s, d)

	if err != nil {
		t.Errorf("Error returned from NewChatMessage: %s", err)
	}

	msg_string := "test"
	content := []byte(msg_string)
	ct := "text/plain"

	var p *model.ChatMessagePart
	p, err = model.NewChatMessagePart(ct, content)

	if err != nil {
		t.Errorf("Error returned from NewChatMessagePart: %s", err)
	}
	m.AddPart(p)

	DisplayChatMessage(*m, c)

	expected_view := fmt.Sprintf("%s: %s\n", s.String(), msg_string)

	if c.String() != expected_view {
		t.Errorf("Message not displayed in console correctly: %s", c.String())
	}
}

func TestChatConsole(t *testing.T) {
	input_ch := make(chan string)
	test_string := "test"
	c := bytes.NewBufferString(fmt.Sprintf("%s\n", test_string))

	go ChatConsole(input_ch, c)

	valid_msg := false
	// TODO: user timer pattern so as not to block forever
	for {
		test_input, ok := <-input_ch
		if test_input == test_string {
			valid_msg = true
		}
		if !ok {
			if !valid_msg {
				t.Errorf("Message not sent before channel closed.")
			}
			return
		}
	}
}
