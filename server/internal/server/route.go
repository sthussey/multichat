package server

import (
	"github.com/oklog/ulid/v2"
	"gitlab.com/sthussey/multichat/server/pkg/model"
	"net"
	"sync"
)

type MessageRouter struct {
	RouterId   ulid.ULID
	RouterAddr net.TCPAddr
}

type MessageEndpoint struct {
	Receiver ulid.ULID
	Router   *MessageRouter
	Messages     chan RoutableMessage
}

type RoutableMessage interface {
  Destination() ulid.ULID
  Source() ulid.ULID
  Id() ulid.ULID
}

// TODO: need to inject config here
func StartRouter() (chan MessageEndpoint, chan RoutableMessage, error) {
	ep_reg := make(chan MessageEndpoint)
	msg_route := make(chan RoutableMessage)
	go registerEndpoints(ep_reg, msg_route)
	return ep_reg, msg_route, nil
}

func registerEndpoints(ep_reg chan MessageEndpoint, msg_route chan RoutableMessage) {
	endpoint_registry := make(map[ulid.ULID]MessageEndpoint)
	ep_reg_lock := sync.RWMutex{}

	go routeMessages(msg_route, endpoint_registry, ep_reg_lock)

	for {
		ep := <-ep_reg
		ep_reg_lock.Lock()
		endpoint_registry[ep.Receiver] = ep
		ep_reg_lock.Unlock()
	}

	return
}

// TODO: need a context for goroutine control
func routeMessages(msg_route chan RoutableMessage, reg map[ulid.ULID]MessageEndpoint, l sync.RWMutex) {
	for {
		msg := <-msg_route
		l.RLock()
		receiver, ok := reg[msg.Destination()]
		l.RUnlock()
		if !ok {
			l.RLock()
			receiver, ok = reg[msg.Source()]
			l.RUnlock()
			if !ok {
				// should log a message
				continue
			}
			resp := model.NewDeliveryFailureMessage(msg.Id())
			receiver.Messages <- &resp
			continue
		}
		receiver.Messages <- msg
	}
	return
}
