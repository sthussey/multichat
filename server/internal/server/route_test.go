package server

import (
  "sync"
  "testing"
  "time"
  "github.com/oklog/ulid/v2"
  "gitlab.com/sthussey/multichat/server/pkg/model"
)

func TestRouteMessages(t *testing.T) {
  msgc_a := make(chan RoutableMessage)
  id_a, _ := ulid.New(ulid.Now(), nil)
  ep_a := MessageEndpoint{Receiver: id_a, Messages: msgc_a}

  msgc_b := make(chan RoutableMessage)
  id_b, _ := ulid.New(ulid.Now(), nil)
  ep_b := MessageEndpoint{Receiver: id_b, Messages: msgc_b}

  reg := make(map[ulid.ULID]MessageEndpoint)
  reg[id_a] = ep_a
  reg[id_b] = ep_b

  l := sync.RWMutex{}
  to_route := make(chan RoutableMessage)

  go routeMessages(to_route, reg, l)

  msg, _ := model.NewChatMessage(id_a, id_b)

  timeout := time.After(5 * time.Second)
  done := make(chan bool)

  go func() {
    to_route <- msg
    recv := <-msgc_b
    if recv.Id() != msg.Id() {
      t.Errorf("Received unexpected message: %s", recv.Id())
    }
    done <- true
  }()

  select {
    case <-timeout:
      t.Fatal("Test timed out before finishing.")
    case <-done:
  }
}

