package server

import (
	"context"
	"fmt"
  "log"
	"github.com/oklog/ulid/v2"
	pb "gitlab.com/sthussey/multichat/server/interface"
	"gitlab.com/sthussey/multichat/server/pkg/model"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
	"io"
)

type multiChatServer struct {
	pb.UnimplementedMultiChatServer
  // channel to register new message endpoint
  ep_reg chan MessageEndpoint
  // channel to send messages for the router to route
  msg_route chan RoutableMessage
  logger  *log.Logger
}

func CreateServer(s *grpc.Server, ep_reg chan MessageEndpoint, msg_route chan RoutableMessage, logger *log.Logger) {
  svr := newServer()
  svr.ep_reg = ep_reg
  svr.msg_route = msg_route
  svr.logger = logger
	pb.RegisterMultiChatServer(s, svr)
}

func newServer() *multiChatServer {
	s := &multiChatServer{}
	return s
}

func (s *multiChatServer) RegisterEndpoint(client_id ulid.ULID) chan RoutableMessage {
  s.logger.Printf("Endpoint Registered for Client ID: %s\n", client_id.String())
  // channel this endpoint will receive messages to deliver to the client
  client_msg := make(chan RoutableMessage)
  client_ep := MessageEndpoint{Receiver: client_id, Router: nil, Messages: client_msg}
  s.ep_reg <-client_ep
  return client_msg
}

func (s *multiChatServer) StartStream(stream pb.MultiChat_StartStreamServer) error {
	s.logger.Printf("StartStream initiated.\n")
  md, _ := metadata.FromIncomingContext(stream.Context())
  client_id, err := ulid.Parse(md["client_id"][0])
  if err != nil {
    return fmt.Errorf("Invalid client_id MD: %v", err)
  }

  msg_read := s.RegisterEndpoint(client_id)
  recv_exit := make(chan struct{})

  // goroutine to accept incoming messages from client and forward to the router
  // TODO: replace recv_exit with a channel to accept control messages
  go func(){
    for {
      in, err := stream.Recv()
		  if err == io.EOF {
			  s.logger.Printf("Debug: client closing connection.\n")
			  close(recv_exit)
        return
		  }
		  if err != nil {
			  s.logger.Printf("Error: could not collect message: %v\n", err)
			  close(recv_exit)
        return
      }
      msg, err := model.ExtractChatMessageFromFrame(*in)
      if err != nil {
        s.logger.Printf("Frame did not contain a chat message: %v\n", err)
        continue
      }
      s.msg_route <- msg
    }
  }()

  for {
    select {
      case <-recv_exit:
        return nil
      case msg := <-msg_read:
        switch msgt := msg.(type) {
          case *model.ChatMessage:
            frame, _ := msgt.Frame()
			      stream.Send(frame)
          default:
            s.logger.Printf("Debug: Not a chat message, ignore.\n")
        }
		}
	}
}

func (s *multiChatServer) CollectMessages(p *pb.CollectMessageParams, stream pb.MultiChat_CollectMessagesServer) error {
	//p is empty for now but required for gRPC generation
	s.logger.Printf("CollectMessages initiated.\n")
	msg := generateTestMessage()
	if msg != nil {
		gmsg, err := msg.ToGrpc()
		if err != nil {
			return err
		}
		payload := pb.ChatFrame_ChatMsg{ChatMsg: gmsg}
		stream.Send(&pb.ChatFrame{Message: &payload})
	}
	return nil
}

func (s *multiChatServer) SendMessage(ctx context.Context, msg *pb.ChatFrame) (*pb.ChatFrame, error) {
	ret, err := processChatFrame(msg)
	return ret, err
}

func processChatFrame(f *pb.ChatFrame) (*pb.ChatFrame, error) {
	ret := pb.ChatFrame{}
	switch m := f.Message.(type) {
	case *pb.ChatFrame_ChatMsg:
		chat_r, err := processChatMessage(m.ChatMsg)
		if err != nil {
			return nil, fmt.Errorf("Error processing chat message: %s", err)
		}
		payload := pb.ChatFrame_ChatMsg{ChatMsg: chat_r}
		ret.Message = &payload
	case *pb.ChatFrame_ControlMsg:
		ctrl_r, err := processControlMessage(m.ControlMsg)
		if err != nil {
			return nil, fmt.Errorf("Error processing control message: %s", err)
		}
		payload := pb.ChatFrame_ControlMsg{ControlMsg: ctrl_r}
		ret.Message = &payload
	}
	return &ret, nil
}

func processChatMessage(msg *pb.ChatMessage) (*pb.ChatMessage, error) {
	rmsg := generateTestMessage()
	if rmsg != nil {
		gmsg, err := rmsg.ToGrpc()
		if err != nil {
			return nil, err
		}
		return gmsg, nil
	}
	return nil, fmt.Errorf("Error generating test message.")
}

func generateTestMessage() *model.ChatMessage {
	s, _ := ulid.New(ulid.Now(), nil)
	d, _ := ulid.New(ulid.Now(), nil)
	m, err := model.NewChatMessage(s, d)
	if err != nil {
		return nil
	}
	var p *model.ChatMessagePart
	p, err = model.NewChatMessagePart("text/plain", []byte("pong!"))
	if err != nil {
		return nil
	}
	m.AddPart(p)
	return m
}

func processControlMessage(msg *pb.ControlMessage) (*pb.ControlMessage, error) {
	return nil, nil
}
