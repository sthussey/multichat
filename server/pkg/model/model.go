package model

import (
	"fmt"
	"github.com/oklog/ulid/v2"
	pb "gitlab.com/sthussey/multichat/server/interface"
	"strings"
)

// TODO: should alias the type for Id to make replacement easy

type ChatUser struct {
	Id          ulid.ULID
	Name        string
	Status      string
	Deliverable bool
	Connected   bool
}

type ChatMessagePart struct {
	contentType string
	content     []byte
}

func (p *ChatMessagePart) Content() []byte {
	if p.content != nil && len(p.content) > 0 {
		return p.content
	} else {
		return nil
	}
}

func (p *ChatMessagePart) ContentType() string {
	return p.contentType
}

func (p *ChatMessagePart) String() string {
	if strings.Index(p.contentType, "text/plain") == 0 {
		return string(p.content)
	} else {
		return fmt.Sprintf("Object of type %s", p.contentType)
	}
}

func NewChatMessagePart(t string, c []byte) (*ChatMessagePart, error) {
  if t == "" || c == nil {
    return nil, fmt.Errorf("Invalid parameters for constructor.")
  }
	p := new(ChatMessagePart)
	p.contentType = t
	p.content = c
	return p, nil
}

func ChatMessagePartFromGrpc(gmp pb.ChatMessagePart) (*ChatMessagePart, error) {
  return NewChatMessagePart(gmp.GetContentType(), gmp.GetContent())
}

type ChatMessage struct {
  id          ulid.ULID
	destination ulid.ULID
	source      ulid.ULID
	parts       []ChatMessagePart
}

func NewChatMessage(s ulid.ULID, d ulid.ULID) (*ChatMessage, error) {
  id, _ := ulid.New(ulid.Now(), nil)
	m := ChatMessage{id: id, destination: d, source: s, parts: nil}
	return &m, nil
}

func ExtractChatMessageFromFrame(gf pb.ChatFrame) (*ChatMessage, error) {
	switch m := gf.Message.(type) {
	case *pb.ChatFrame_ChatMsg:
    msg, err := ChatMessageFromGrpc(*m.ChatMsg)
		if err != nil {
			return nil, fmt.Errorf("Error processing chat message: %s", err)
		}
		return msg, nil
	//case *pb.ChatFrame_ControlMsg:
  default:
		return nil, fmt.Errorf("Frame does not contain chat message.")
	}
}

func ChatMessageFromGrpc(gmsg pb.ChatMessage) (*ChatMessage, error) {
  src, err := ulid.Parse(gmsg.GetSource().GetId())
  if err != nil {
    return nil, fmt.Errorf("Error parsing Source id: %v", err)
  }
  dest, err := ulid.Parse(gmsg.GetDestination().GetId())
  if err != nil {
    return nil, fmt.Errorf("Error parsing Destination id: %v", err)
  }
  msg, err := NewChatMessage(src, dest)
  for _, gp := range gmsg.Message {
    p, err := ChatMessagePartFromGrpc(*gp)
    if err != nil {
      // log error
      continue
    }
    msg.AddPart(p)
  }
  return msg, nil
}

func (m *ChatMessage) Id() ulid.ULID {
  return m.id
}

func (m *ChatMessage) Destination() ulid.ULID {
	return m.destination
}

func (m *ChatMessage) SetDestination(d ulid.ULID) error {
	m.destination = d
	return nil
}

func (m *ChatMessage) Source() ulid.ULID {
	return m.source
}

func (m *ChatMessage) SetSource(s ulid.ULID) error {
	m.source = s
	return nil
}

func (m *ChatMessage) Parts() []ChatMessagePart {
	p := make([]ChatMessagePart, len(m.parts))
	copy(p, m.parts)
	return p
}

func (m *ChatMessage) AddPart(p *ChatMessagePart) error {
	if m.parts == nil {
		m.parts = make([]ChatMessagePart, 0)
	}
	m.parts = append(m.parts, *p)
	return nil
}

func (m *ChatMessage) ClearParts() error {
	m.parts = make([]ChatMessagePart, 1)
	return nil
}

func (m *ChatMessage) ToGrpc() (*pb.ChatMessage, error) {
	gm := pb.ChatMessage{
		Destination: &pb.Endpoint{Id: m.Destination().String()},
		Source:      &pb.Endpoint{Id: m.Source().String()},
		Message:     make([]*pb.ChatMessagePart, 0)}
	for _, p := range m.Parts() {
		gmp, err := p.ToGrpc()
		if err != nil {
			return nil, fmt.Errorf("Error converting ChatMessagePart: %v\n", err)
		}
		gm.Message = append(gm.Message, gmp)
	}
	return &gm, nil
}

// Create a gRPC chatframe containing this chatmessage
func (m *ChatMessage) Frame() (*pb.ChatFrame, error) {
  gmsg, err := m.ToGrpc()
	if err != nil {
		return nil, err
	}
	payload := pb.ChatFrame_ChatMsg{ChatMsg: gmsg}
  frame := pb.ChatFrame{Message: &payload}
  return &frame, nil
}

func (mp *ChatMessagePart) ToGrpc() (*pb.ChatMessagePart, error) {
	gmp := pb.ChatMessagePart{
		Content:     make([]byte, len(mp.Content())),
		ContentType: mp.ContentType()}
	copy(gmp.Content, mp.Content())
	return &gmp, nil
}

type ControlMessage interface {
  Id() ulid.ULID
  Destination() ulid.ULID
  SetDestination(ulid.ULID) error
  Source() ulid.ULID
  SetSource(ulid.ULID) error
	Subject() ulid.ULID
	SetSubject(ulid.ULID) error
	Command() string
	SetCommand(string) error
	Parameters() map[string]interface{}
	Parameter(string) (interface{}, error)
	SetParameter(string, interface{}) error
}

type controlCommon struct {
  id  ulid.ULID
  destination ulid.ULID
  source  ulid.ULID
	subject    ulid.ULID
	command    string
	parameters map[string]interface{}
}

func (m *controlCommon) Id() ulid.ULID {
  return m.id
}

func (m *controlCommon) Destination() ulid.ULID {
	return m.destination
}

func (m *controlCommon) SetDestination(d ulid.ULID) error {
	m.destination = d
	return nil
}

func (m *controlCommon) Source() ulid.ULID {
	return m.source
}

func (m *controlCommon) SetSource(s ulid.ULID) error {
	m.source = s
	return nil
}

func (m *controlCommon) Subject() ulid.ULID {
	return m.subject
}

func (m *controlCommon) SetSubject(s ulid.ULID) error {
	m.subject = s
	return nil
}

func (m *controlCommon) Command() string {
	return m.command
}

func (m *controlCommon) SetCommand(c string) error {
	m.command = c
	return nil
}

func (m *controlCommon) Parameters() map[string]interface{} {
	return m.parameters
}

func (m *controlCommon) Parameter(p string) (interface{}, error) {
	if m.parameters == nil {
		return nil, fmt.Errorf("Parameter %s not set.", p)
	}
	v, ok := m.parameters[p]
	if !ok {
		return nil, fmt.Errorf("Parameter %s not set.", p)
	}
	return v, nil
}

func (m *controlCommon) SetParameter(p string, v interface{}) error {
	if m.parameters == nil {
		m.parameters = make(map[string]interface{})
	}
	m.parameters[p] = v
	return nil
}

type UserStatusMessage struct {
	controlCommon
}

func (m *UserStatusMessage) SetParameter(p string, v interface{}) error {
	if p == "description" || p == "accepting" {
		_ = m.controlCommon.SetParameter(p, v)
		return nil
	}
	return fmt.Errorf("Unknown parameter %s.", p)
}

type DeliverySuccessMessage struct {
  controlCommon
}

// TODO: command should probably be an enum
func NewDeliverySuccessMessage(id ulid.ULID) DeliverySuccessMessage {
  m := DeliverySuccessMessage{}
  m.SetSubject(id)
  m.SetCommand("dlvr_ok")
  return m
}

type DeliveryFailureMessage struct {
  controlCommon
}

// TODO: command should probably be an enum
func NewDeliveryFailureMessage(id ulid.ULID) DeliveryFailureMessage {
  m := DeliveryFailureMessage{}
  m.SetSubject(id)
  m.SetCommand("dlvr_fail")
  return m
}

func (m *DeliveryFailureMessage) SetParameter(p string, v interface{}) error {
  return fmt.Errorf("Unknown parameter %s.", p)
}

