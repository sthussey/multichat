package model

import (
	"bytes"
	"fmt"
	"github.com/oklog/ulid/v2"
	pb "gitlab.com/sthussey/multichat/server/interface"
	"testing"
)

func TestNewChatMessagePart(t *testing.T) {
	c := []byte{'t', 'e', 's', 't'}
	ct := "text/plain"

	p, err := NewChatMessagePart(ct, c)

	if err != nil {
		t.Errorf("ChatMessagePart constructor failed: %s", err)
	}

	if p.ContentType() != ct {
		t.Errorf("ChatMessagePart constructor failed to set ContentType")
	}

	if bytes.Compare(p.Content(), c) != 0 {
		t.Errorf("ChatMessagePart constructor failed to set Content")
	}
}

func TestChatMessagePartStringer(t *testing.T) {
	ct := "text/plain"
	c := []byte{'a', 'b', 'c'}

	p, err := NewChatMessagePart(ct, c)

	if err != nil {
		t.Errorf("Error instantiating ChatMessagePart")
	}

	if p.String() != string(c) {
		t.Errorf("ChatMessagePart stringer not working for text type.")
	}

	ct = "image/jpeg"
	c = []byte{0}

	p, err = NewChatMessagePart(ct, c)

	if p.String() != fmt.Sprintf("Object of type %s", ct) {
		t.Errorf("ChatMessagePart string not working for non-text type.")
	}
}

func TestNewChatMessage(t *testing.T) {
	s, _ := ulid.New(ulid.Now(), nil)
	d, _ := ulid.New(ulid.Now(), nil)
	m, err := NewChatMessage(s, d)
	if err != nil {
		t.Errorf("Error returned from NewChatMessage: %s", err)
	}

	if s != m.Source() {
		t.Errorf("ChatMessage constructor did not correctly set source.")
	}

	if d != m.Destination() {
		t.Errorf("ChatMessage construct did not correctly set destination.")
	}

	ct := "text/plain"
	c := []byte{'a', 'b', 'c'}

	p, err := NewChatMessagePart(ct, c)

	if err != nil {
		t.Errorf("Error creating test MessagePart: %v", err)
	}

	m.AddPart(p)

	if len(m.parts) != 1 {
		t.Errorf("Single part message has %d parts.", len(m.parts))
	}
}

func TestChatMessagePartGrpc(t *testing.T) {
	c := []byte{'t', 'e', 's', 't'}
	ct := "text/plain"

	p, err := NewChatMessagePart(ct, c)

	if err != nil {
		t.Errorf("ChatMessagePart constructor failed: %s", err)
	}

	var gp *pb.ChatMessagePart
	gp, err = p.ToGrpc()

	if err != nil {
		t.Errorf("Error converting ChatMessagePart model to grpc: %v\n", err)
	}

	if !bytes.Equal(p.Content(), gp.Content) {
		t.Errorf("Conversion mangled Content field.")
	}

	if p.ContentType() != gp.ContentType {
		t.Errorf("Conversion mangled ContentType field.")
	}
}

func TestChatMessageToGrpc(t *testing.T) {
	s, _ := ulid.New(ulid.Now(), nil)
	d, _ := ulid.New(ulid.Now(), nil)
	m, err := NewChatMessage(s, d)

	if err != nil {
		t.Errorf("Error returned from NewChatMessage: %s", err)
	}

	c := []byte{'t', 'e', 's', 't'}
	ct := "text/plain"

	var p *ChatMessagePart
	p, err = NewChatMessagePart(ct, c)

	if err != nil {
		t.Errorf("Error returned from NewChatMessagePart: %s", err)
	}

	m.AddPart(p)

	var gm *pb.ChatMessage
	gm, err = m.ToGrpc()

	if s.String() != gm.Source.Id {
		t.Errorf("Conversion mangled Source field.")
	}

	if d.String() != gm.Destination.Id {
		t.Errorf("Conversion mangled Destination field.")
	}

	if len(gm.Message) != 1 {
		t.Errorf("Conversion mangled ChatMessagePart field: length of %d", len(gm.Message))
	}
}
