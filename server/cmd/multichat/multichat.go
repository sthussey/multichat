package main

import (
  "log"
	server "gitlab.com/sthussey/multichat/server/internal/server"
	"google.golang.org/grpc"
	"net"
  "os"
)

func main() {
  logger := log.New(os.Stdout, "", log.LstdFlags | log.Lshortfile | log.LUTC)

	var opts []grpc.ServerOption
	lis, err := net.Listen("tcp", "localhost:8088")
  if err != nil {
    logger.Printf("Error starting server: %v\n", err)
    os.Exit(1)
  }

  ep_reg, route_msg, err := server.StartRouter()

  if err != nil {
    logger.Printf("Error starting message router: %v\n", err)
    os.Exit(2)
  }

	grpcServer := grpc.NewServer(opts...)
	server.CreateServer(grpcServer, ep_reg, route_msg, logger)
	grpcServer.Serve(lis)
}
