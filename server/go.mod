module gitlab.com/sthussey/multichat/server

go 1.13

require (
	github.com/oklog/ulid/v2 v2.0.2
	google.golang.org/grpc v1.25.1
)
